# Docker2Restic-Backup

### S3 backup up whole systems and their docker volumes from inside containers , dumping mysql postgresql  with rsyncable zips

* backup is done from "container view" , since /var/lib/docker/volumes is inconsistent
* logs to /var/log/docker2restic.{system,volumes,mysql,pgsql}.log
* mainly developed for custom s3 /minio targets, should work with other backends as well 
* is able to send a notification about the backup if you have a file /etc/telegram-notify.backup.debug.conf ( se below)
   (example:)

## you need:
* /etc/backup.restic.conf ( see example folder) OR  environment variables set ( see below )
* access to the docker socket
* jq , restic v0.9+ and optionally docker  (  system will try to auto-install necessary tools)
* for automatic pruning of deleted containers you need aws cli ( https://github.com/awslabs/aws-shell ) so python-pip or python (see below) is necessary
* the status monitor needs lsof installed
* see below how to initially install restic
* the restic-fantastic docker image will be used in addition to your system-wide restic

## you get:
* the following example shows a structure of a resulting s3 bucket 
   **note:** due to GDPR requirements ( especially if customers need their backups deleted etc ) each container is a single repository aside from "system" 
   the name of the repo is taken from the actual docker `container-name` , so a split fpm/webhost project WILL have 2 entries , all detected database containers
   are separated from the volumes , if a database is not detected , it's volumes will be saved
   ```
   |-docker
   | ├── database
   | │   ├── database.someproject.my.domain.example
   | │   │   ├── config
   | │   │   ├── data
   | │   │   ├── index
   | │   │   ├── keys
   | │   │   └── snapshots
   | │   └── mailthing.my.domain.example
   | │       ├── config
   | │       ├── data
   | │       ├── index
   | │       ├── keys
   | │       └── snapshots
   | └── volumes
   |     ├── mailthing.my.domain.example
   |     |   ├── config
   |     |   ├── data
   |     |   ├── index
   |     |   ├── keys
   |     |   └── snapshots
   |     ├── someproject.my.domain.example
   |     |   ├── config
   |     |   ├── data
   |     |   ├── index
   |     |   ├── keys
   |     ├── auth.my.domain.example
   |     |   ├── config
   |     |   ├── data
   |     |   ├── index
   |     |   ├── keys
   |     ├── php-fpm.someproject.my.domain.example
   |     │   ├── config
   |     │   └── keys
   |     └── redis.someproject.my.domain.example
   |         ├── config
   |         └── keys
   |-system
   | ├── config
   | ├── data
   | ├── index
   | ├── keys
   | ├── locks
   | └── snapshots
   | 
   └─system-mysql/
     └── system
         ├── config
         ├── data
         ├── index
         ├── keys
         └── snapshots
   ```
   

unfortunately restic is very ancient in the default repositories of certain OS (i.e. v~0.3 in ubuntu 18.04 ) , so better get it from their github site https://github.com/restic/restic/releases , they even offer arm and arm64

(quick install example : PLEASE SEARCH MOST RECENT RESTIC URL BEFORE and use a version that supports self update !! 


``` cd /tmp/ && wget -c "https://github.com/awslabs/aws-shell/archive/master.zip" -O aws-cli.zip && unzip aws-cli-zip && cd aws-shell-master/ && python setup.py install ```

``` cd /tmp/ && wget -c "https://github.com/restic/restic/releases/download/v0.9.6/restic_0.9.6_linux_amd64.bz2"  && bunzip2 restic_0.9.6_linux_amd64.bz2 && chmod +x restic_0.9.6_linux_amd64 && mv restic_0.9.6_linux_amd64 /usr/bin/restic ```

)

## Environment variables

`RESTIC_AWS_URL` → e.g s3:https://my.s3.domain/my-s3-bucket

`AWS_ACCESS_KEY_ID`

`AWS_SECRET_ACCESS_KEY`

`RESTIC_PASSWORD`

---


## Webhook/telegram notification

You just need the config file 

## USING webhoks2telegram 
### ATTENTION: the webhook2telegram  was renamed( to telepush ) and refactored, the PAM-script is tested with all versions
### but relies on the fact that the newest version "telepush" returns an error when using the "old" api/messages endpoint
### docker image tags: v1 n1try/webhook2telegram | v2 ghcr.io/muety/webhook2telegram:latest |v3 ghcr.io/muety/telepush
### v2 Link       https://github.com/muety/telepush/pkgs/container/webhook2telegram
### refactoring   https://github.com/muety/telepush/releases/tag/3.0.0

* ( first setup a signal bot , example docker-compose below)
* create  /etc/pam-tg-webhook.conf 
   ```
   export TGWEBBHOOKTOK=1f2f3f4f-3af4-4655-baac-11aaaaabbccc
   export TGWEBBHOOKURL=https://signals.your.dmomain/api/messages
   ```


* example signals bot compose file: 

```
networks:
  default:
    external:
      name: watchdog
services:
  webhook2telegram:
    container_name: signals.watch.yourdomain.lan
    environment:
      APP_MODE: longpolling
      APP_TOKEN: 1234567890:AABcAAAhAaaaa4AAaA_aaaaaaaaa111111
      APP_URL: signals.watch.yourdomain.lan
      LETSENCRYPT_EMAIL: ${LETSENCRYPT_EMAIL:-default-mail-not-set@using-fallback-default.slmail.me}
      LETSENCRYPT_HOST: signals.watch.yourdomain.lan
      NGINX_NETWORK: watchdog
      SSH_PORT: ''
      SSL_POLICY: Mozilla-Modern
      VIRTUAL_HOST: signals.watch.yourdomain.lan
      VIRTUAL_PORT: 8080
      VIRTUAL_PROTO: http
    hostname: signals.watch.yourdomain.lan
#    image: n1try/webhook2telegram
#    image: ghcr.io/muety/webhook2telegram:latest
    image: ghcr.io/muety/telepush    logging:
      driver: json-file
      options:
        max-file: '10'
        max-size: 20m
    ports:
    - 127.0.0.1:55553:8080/tcp
    restart: unless-stopped
    volumes:
    - /storage_global/data/signals.watch.yourdomain.lan/botdata:/srv/data:rw
version: '3.0'
```   

* example .env for the signals bot 
```
APP_URL=signals.watch.yourdomain.lan
APP_MODE=webhook
APP_MODE=longpolling
APP_TOKEN=1234567890:AABcAAAhAaaaa4AAaA_aaaaaaaaa111111
NGINX_NETWORK=watchdog
```

 **hint: bots in multi user chats only see messages beginning with slash ("/") , until you change privacy mode by messaging botmaster so send e.g. "/init" to see it in getUpdates**

---


TODO:

* xtrabackup where possible
* further improve oprhan deletion ( aws-cli)
* influxdb backup (uses file storage)---
---

<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker2restic-backup/README.md/logo.jpg" width="480" height="270"/></div></a>
