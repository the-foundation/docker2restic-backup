#!/bin/bash


##todo
## :subprocess ssh: Permission denied, please try again
## Permission denied (publickey,password)
## Fatal: unable to open repo
SUMMARY=""
which docker &>/dev/null && docker pull thefoundation/restic-fantastic &>/dev/shm/restic.fanstastic.dockerpull.log

function send_gotify() { 
    # args: endpoint title message prio
    ## you can use a broadcast endpoint like "https://gotify.your.host.lan/plugin/1/custom/ABCDEFG123__/message?channel=backupsfromremote"
    [[ -z "$PRIO" ]] && PRIO=0
    
    [[ ! -z "$PRIO" ]] && [[ ! -z "$3" ]] && [[ ! -z "$2" ]] && [[ ! -z "$1" ]]  && {
		REFUSEDOPT=$(curl --help |grep -q "retry-connrefused" && echo "--retry-connrefused" )
        curl --retry 3 $REFUSEDOPT --retry-delay 123 --retry-max-time 1200 --connect-timeout 45 "$1"  -F "title=$2"  -F "message=$3" -F "priority=$PRIO" ; } ; 
echo -n ; }; 

function send_tgwebhook_file() {

# args: origin txt filename
#With the release of version 3, Webhook2Telegram was rebranded to Telepush 
# Recipient token is now encoded as a path parameter, instead of included inside the payload. Routes have changed, e.g. from /api/messages to /api/messages/<recipient>, while the recipient_token field was removed from a message's JSON schema

[[ ! -z "${TGWEBBHOOKURL}" ]] && [[ ! -z "${TGWEBBHOOKTOK}" ]] && [[ ! -z "$2" ]] && [[ ! -z "$1" ]] && {
    verify="-k"
    [[ "VERIFY_SSL" = "true" ]] && verify=""
        tgtxt="$2"
    tgtxt=${tgtxt//\'/}
    tgtxt=${tgtxt//\"/}
echo > /dev/shm/tgbackupwebhook.log

 ## send the file
 test -e /dev/shm/.DEBUG.TG &>/dev/null && ( echo '{
    "recipient_token": "SCRUBBED",
    "file": "base64 -w 0 '$3'",
    "filename": "'$(basename "$3")'",
    "type": "FILE",
    "origin": "'$1'"
}' > /dev/shm/.DEBUG.TG.FILE.json ; chmod go-rw /dev/shm/.DEBUG.TG.FILE.json )

REFUSEDOPT=$(curl --help |grep -q "retry-connrefused" && echo "--retry-connrefused" )
(
##file
## v2
echo '{
"recipient_token": "'${TGWEBBHOOKTOK}'",
"file": "'$(base64 -w 0 "$3")'",
"filename": "'$(basename "$3")'",
"type": "FILE",
"origin": "'$1'"
}' | curl --retry 3 $REFUSEDOPT --retry-delay 123 --retry-max-time 1200 --connect-timeout 45 --fail -Lv $verify -X POST ${TGWEBBHOOKURL} -H "Content-Type: application/json" --data-binary @/dev/stdin &> /dev/shm/tgbackupwebhook.log

) || (
## v3
echo '{
"file": "'$(base64 -w 0 "$3")'",
"filename": "'$(basename "$3")'",
"type": "FILE",
"origin": "'$1'"
}' | curl --retry 3 $REFUSEDOPT --retry-delay 123 --retry-max-time 1200 --connect-timeout 45 --fail -Lv $verify -X POST ${TGWEBBHOOKURL}/${TGWEBBHOOKTOK} -H "Content-Type: application/json" --data-binary @/dev/stdin &>> /dev/shm/tgbackupwebhook.log
 )
 
## send the text 
test -e /dev/shm/.DEBUG.TG &>/dev/null && ( echo '{
"text": "'$tgtxt'",
"type": "TEXT",
"origin": "'$1'"
}' >> /dev/shm/.DEBUG.TG.FILE.json ; chmod go-rw /dev/shm/.DEBUG.TG.FILE.json )

 (
##txt
## v2
echo '{
"recipient_token": "'${TGWEBBHOOKTOK}'",
"text": "'$tgtxt'",
"type": "TEXT",
"origin": "'$1'"
}' | curl --retry 3 $REFUSEDOPT --retry-delay 123 --retry-max-time 1200 --connect-timeout 45 --fail -Lv $verify -X POST ${TGWEBBHOOKURL} -H "Content-Type: application/json" --data-binary @/dev/stdin  &>> /dev/shm/tgbackupwebhook.log

) || (
## v3
echo '{
"text": "'$tgtxt'",
"type": "TEXT",
"origin": "'$1'"
}' | curl --retry 3 $REFUSEDOPT --retry-delay 123 --retry-max-time 1200 --connect-timeout 45 	--fail -Lv $verify -X POST ${TGWEBBHOOKURL}/${TGWEBBHOOKTOK} -H "Content-Type: application/json" --data-binary @/dev/stdin &>> /dev/shm/tgbackupwebhook.log
 )   


chmod go-rwx /dev/shm/tgbackupwebhook.log;

echo -n ; } ;
echo -n ; } ;




#CPULIMIT_OPTIONS=$( ( cpulimit --help |grep -q -- '--monitor-forks' && echo ' --monitor-forks ';cpulimit --help |grep -q -- '--foreground' && echo ' --foreground ' ; cpulimit --help |grep -q -- '--quiet' && echo ' --quiet ' ) |tr -d '\n' )
CPULIMIT_OPTIONS=$( ( cpulimit --help |grep -q -- '--foreground' && echo ' --foreground ' ; cpulimit --help |grep -q -- '--quiet' && echo ' --quiet ' ) |tr -d '\n' )

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$PATH

## save flag if overwritten in config
CMDLINEDEBUG=false
[[ "$DEBUGME" = "true" ]] &&  CMDLINEDEBUG=true
CMDLINEDEBUGHARDER=false
[[ "$DEBUGMEHARDER" = "true" ]] &&  CMDLINEDEBUGHARDER=true

[[ "$DEBUGME" = "true" ]] && echo ":pre:config" >&2
test -f /etc/backup.restic.conf && source /etc/backup.restic.conf
##restore debug state if cmdlineset
[[ "$CMDLINEDEBUG" = "true" ]] && DEBUGME=true
[[ "$CMDLINEDEBUGHARDER" = "true" ]] && DEBUGMEHARDER=true

[[ "$RESTIC_NOCACHE" = "true" ]] && NO_CACHE=" --no-cache "


##    run : export DEBUGME=true  before
## OR run : DEBUGME=true /bin/bash run-backup.sh
[[ -z "${DEBUGME}" ]] && DEBUGME=false

[[ "$DEBUGME" = "true" ]] && echo ":pre:sshconfig" >&2

echo "StrictHostKeyChecking=accept-new" > /dev/shm/restic_ssh_config

[[ "$DEBUGME" = "true" ]] && echo ":pre:apk" >&2

# alpine: grep does not allow --line-buffered , sed does not allow: -u (unbuffered)
which apk &>/dev/null  && apk add grep sed util-linux cpulimit gzip jq &>/dev/null
_apt_install_if_missing() { which "$1" &>/dev/null || apt-get &>/dev/null  && { apt-get update &>/dev/null ;apt-get install "$1"  ; } ;
     echo -n  ; } ;



[[ "$DEBUGME" = "true" ]] && echo ":pre:aws" >&2

## aws cli v1 install block

which aws &>/dev/null || {
  which apt-get &>/dev/null && apt-get update &>/dev/null && apt-get install -y --no-install-recommends awscli &>/dev/null
  which apk &>/dev/null && {
    apk add --no-cache \
            python3 \
            py3-pip &>/dev/null && \
        pip3 install --upgrade pip  &>/dev/null && \
        pip3 install && \
            awscli &>/dev/null &&  \
        find /var/cache/apk/ -type f -delete ;
    echo -n >/dev/null ;};
  echo -n >/dev/null ;};



### END PREINSTALL
[[ "$DEBUGME" = "true" ]] && echo ":pre:cpulim" >&2

export HAVE_CPULIMIT=false
which cpulimit &>/dev/null &&                         { export HAVE_CPULIMIT=true; HAVE_CPULIMIT=true ; } ;
which cpulimit 2>&1 |grep cpulimit |wc -l |grep ^0 || { export HAVE_CPULIMIT=true; HAVE_CPULIMIT=true ; } ;
#echo $HAVE_CPULIMIT

# reduce go memory usage ( https://github.com/restic/restic/issues/1988 )
export GOGC=20
# tell golang to limit only 2CPUS
[[ -z $GOMAXPROCS ]] && export GOMAXPROCS=2
[[ -z $RESTIC_COMPRESSION ]] && export RESTIC_COMPRESSION=auto
[[ -z $RESTIC_READ_CONCURRENCY ]] && export RESTIC_READ_CONCURRENCY=8
[[ -z $RESTIC_PACK_SIZE ]] && export RESTIC_PACK_SIZE=128



## defaults
## keep 30 days
[[ -z ${KEEPDAYS} ]] && KEEPDAYS=30
[[ "$DEBUGME" = "true" ]] && echo ":keepdays:$KEEPDAYS" >&2

_dropcaches() {  sysctl -w vm.drop_caches=1 &>/dev/null ;sync ; } ;

_calculate_loadpenalty() {

## reduce our cpulimit percentage quota by 1/5th of the current mid-term load
###  e.g. : 8 cores  , load 4 ( 400% percent used) -> would reduce a 128% limit to 78% ( 71% to 21% and so on)
MYLOADPENALTYPERCENT=$(awk ' BEGIN { print '$(cat /proc/loadavg |cut -d" " -f2|tr -d '\n' )' *100/5 } ' )
MYLOADPENALTYPERCENT=$(echo ${MYLOADPENALTYPERCENT}|cut -d"." -f1)
## if calculation goes wrong: fallback to 10 % penalty
case $MYLOADPENALTYPERCENT in
    ''|*[!0-9]*) export MYLOADPENALTYPERCENT=10;MYLOADPENALTYPERCENT=10 ;;
    *) [[ "$DEBUGME" = "true" ]] &&  echo good penalty:is a number val $MYLOADPENALTYPERCENT >&2 ;;
esac
echo $MYLOADPENALTYPERCENT  ; } ;

_calculate_cpumaxperc() {

### do not interfere too hard , database dumping and storage creates additional load..
## default 128% (1.28 cores) on 8 threaded machine
## default  71% (0.71 cores) on 4 threaded machine
## default  28% (0.28 cores) on 1 thread   machine -> will be set to 50% below
MYDEFAULTMAXPERC=$(( ( $(nproc) +1 ) *100 / 7 ))

## if calculation goes wrong: fallback to 142 % cpulimit
case $MYDEFAULTMAXPERC in
    ''|*[!0-9]*) export MYDEFAULTMAXPERC=142;MYDEFAULTMAXPERC=142 ;;
    *) [[ "$DEBUGME" = "true" ]] &&  echo good MYDEFAULTMAXPERC:is a number val $MYDEFAULTMAXPERC >&2 ;;
esac

MYDEFAULTMAXPERC=$(($MYDEFAULTMAXPERC-$(_calculate_loadpenalty)))
##if the calculation gave us less than half a core , complete with 50
[[ 50 -ge ${MYDEFAULTMAXPERC} ]] && MYDEFAULTMAXPERC=50

##### check if user wants something else regarding cpulimit ( we set this varable only if empty)
###[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$DEFAULTMAXPERC

echo "$MYDEFAULTMAXPERC" ; } ;
#####

LOADPENALTYPERCENT=$(_calculate_loadpenalty)
############################DEFAULTMAXPERC ...
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")

[[ "$DEBUGME" = "true" ]] && echo ":pre:max_cpu_percent: "${CPUMAXPERC} >&2



[[ "$DEBUGME" = "true" ]] && echo ":pre:logrot" >&2

## ROTATE LOGS first
for logfile in /var/log/docker2restic.{pgsql,mysql,system,volumes}.log;
 do find ${logfile} -mtime -1 2>/dev/null |grep -q ${logfile} && mv ${logfile} ${logfile}_rotated.$(date -u +%F-%H.%M).log ;
done
## delete old ones
find /var/log -maxdepth 1 -name "docker2restic.*_rotated.*log" -mtime +14 -delete &
find /tmp/ -maxdepth 1 -name "docker2resti*.log" -mtime +3 -delete &

_renice_restic() {
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
                    (sleep $1 &>/dev/null ;[[ ! -z "$(pidof restic 2>/dev/null)" ]] && for thispid in $(pidof restic 2>/dev/null) ;do [[ ! -z "$thispid" ]] && test -e /dev/shm/.restic.cpulimit.active.${thispid} || ( touch /dev/shm/.restic.cpulimit.active.${thispid}  ; timeout 1800 cpulimit --limit=${CPUMAXPERC} ${CPULIMIT_OPTIONS//--foreground} -p ${thispid} ;rm /dev/shm/.restic.cpulimit.active.${thispid}   ) &  done  2>&1|grep -v -e "^Process.*dead.*" -e "^Process.*detected"  )    &
                    (sleep $1 &>/dev/null ;[[ ! -z "$(pidof restic 2>/dev/null)" ]] && for thispid in $(pidof restic 2>/dev/null) ;do [[ ! -z "$thispid" ]] && ionice -n 7  -p $thispid 2>&1|grep -v -e "old prio" -e detected$ ;done ) &
                    (sleep $1 &>/dev/null ;[[ ! -z "$(pidof restic 2>/dev/null)" ]] && for thispid in $(pidof restic 2>/dev/null) ;do [[ ! -z "$thispid" ]] && renice -n 7  -p $thispid 2>&1|grep -v -e "old prio" -e detected$ ;done ) &
echo -n ; } ;


[[ "$DEBUGME" = "true" ]] && echo ":pre:func:log" >&2

##FUNCTIONS HEADER
_logger_lastrun() { cat |tr -d '\r' |grep -v ^$|while read line;do
                                                                echo  $(date -u "+%F %T")"@"${STAGE}":"${line} | tee -a /dev/shm/docker2restic.lastrun.log
                                                                        done ; } ;

_logger_pgsql()   { cat |tr -d '\r' |grep -v ^$|while read line;do
                                                                  echo  $(date -u "+%F %T")"@"${STAGE}":"${line} ;
                                                                        done  | tee -a /dev/shm/docker2restic.lastrun.log | tee -a  /var/log/docker2restic.pgsql.log   | tee -a ${mylogdest} >/dev/null ; } ;
_logger_mysql()   { cat |tr -d '\r' |grep -v ^$|while read line;do
                                                                  echo  $(date -u "+%F %T")"@"${STAGE}":"${line} ;
                                                                        done  | tee -a /dev/shm/docker2restic.lastrun.log | tee -a  /var/log/docker2restic.mysql.log   | tee -a ${mylogdest} >/dev/null ; } ;
_logger_volume()  { cat |tr -d '\r' |grep -v ^$|while read line;do
                                        [[ -z "${line// /}" ]] || echo  $(date -u "+%F %T")"@"${STAGE}":"${line}   ;
                                                                        done  | tee -a /dev/shm/docker2restic.lastrun.log | tee -a  /var/log/docker2restic.volumes.log | tee -a ${mylogdest} >/dev/null ; } ;
_logger_sys()     { cat |tr -d '\r' |grep -v ^$|while read line;do
                                        [[ -z "${line// /}" ]] || echo  $(date -u "+%F %T")"@"${STAGE}":"${line}   ;
                                                                        done  | tee -a /dev/shm/docker2restic.lastrun.log | tee -a  /var/log/docker2restic.system.log  | tee -a ${mylogdest} >/dev/null ; } ;
[[ "$DEBUGME" = "true" ]] && echo ":pre:func:util" >&2


_stdout_delete_filter() { grep -e " within " -e "snapshots for "  -e Reason -e keep  |grep -v -e "password is correct" -e "^/" |grep -v -i -e "keep 1 snapsh" ; } ;

## if there is not at least half of the available processing power +1  core in idle (e.g. 5 cores idle with 8 cores total), we sync and then sleep for 42s since we might have caused high load and want the system to calm down
_sleep_on_high_load() {
                      LOADSHORTPERCENT=$(   awk 'BEGIN { print '$(cat /proc/loadavg |cut -d" " -f2 |tr -d '\n')'*100/2 }' );
                      CPUAVAILABLEPERCENT=$(awk 'BEGIN { print '$(nproc)'*100 }');
                      IDLEPERCENT=$(awk "BEGIN { print $CPUAVAILABLEPERCENT - $LOADSHORTPERCENT }");
                      IDLEPERCENT=$(echo "${IDLEPERCENT}"|cut -d"." -f1);
                      [[ "$(nproc)" = "1" ]] || DESIREDIDLEPERCENT=$((($CPUAVAILABLEPERCENT/$(($(nproc)+1-1)))+50))
                      [[ "$(nproc)" = "1" ]] && DESIREDIDLEPERCENT=50
                      [[ "$IDLEPERCENT" -le 100 ]] && DESIREDIDLEPERCENT=50
                      [[ "$DEBUGME" = "true" ]] && echo -en '\r'"comparing CPU% free : "$IDLEPERCENT" ...throttling if less than "$DESIREDIDLEPERCENT
                      #[[ "$DEBUGME" = "true" ]] && echo IDLEPERCENT STARTING
                      [[ ! -z "$(pidof restic 2>/dev/null)" ]] && [[ "$IDLEPERCENT" -le "$DESIREDIDLEPERCENT" ]] && {
                                                                                    [[ "$DEBUGME" = "true" ]] &&  { echo "load too high IDLE: $IDLEPERCENT NEEDED:  $DESIREDIDLEPERCENT LOAD: $LOADSHORTPERCENT OF $CPUAVAILABLEPERCENT " >&2 ; } ;
                                                                                    kill -STOP $(pidof restic 2>/dev/null) &>/dev/null
                                                                                    #_dropcaches & sleep 1;
                                                                                    sleep 1
                                                                                    sleep $(($(cat /proc/loadavg |cut -d"." -f1)-$(nproc))) &>/dev/null
                                                                                    kill -CONT $(pidof restic 2>/dev/null) &>/dev/null
                                                                                    echo -n  ; } ;

## AND YES ,on a single core machine this funtion compares idle (always under 100 ) with 150 and will always _sleep
## AND YES ,on a dual   core machine this funtion compares idle (always under 200 ) with 200 and will always _sleep
## AND YES ,on a quad   core machine this funtion compares idle (always under 400 ) with 300 and will almost always sleep since load should go over 1

 echo -n ; } ;

_stop_restic_on_load() {
  LOADSHORTPERCENT=$(   awk 'BEGIN { print '$(cat /proc/loadavg |cut -d" " -f2|tr -d ' \n')'*100/2 }' );
  CPUAVAILABLEPERCENT=$(awk 'BEGIN { print '$(nproc)'*100 }');
  IDLEPERCENT=$(        awk "BEGIN { print $CPUAVAILABLEPERCENT - $LOADSHORTPERCENT }");
  IDLEPERCENT=$(echo "${IDLEPERCENT}"|cut -d"." -f1);
  [[ "$(nproc)" = "1" ]] || DESIREDIDLEPERCENT=$((($CPUAVAILABLEPERCENT/$(($(nproc)+1-1)))+50))
  [[ "$(nproc)" = "1" ]] && DESIREDIDLEPERCENT=50
  [[ "$IDLEPERCENT" -le 100 ]] && DESIREDIDLEPERCENT=50
  [[ "$DEBUGME" = "true" ]] && echo -en '\r'"comparing CPU% free : "$IDLEPERCENT" ...throttling if less than "$DESIREDIDLEPERCENT
resticpid=$(pidof restic 2>/dev/null)
  [[ "$IDLEPERCENT" -le 100 ]] && DESIREDIDLEPERCENT=50
  [[ "$IDLEPERCENT" -le "$DESIREDIDLEPERCENT" ]] && { [[ "$DEBUGME" = "true" ]] &&  echo "IDLE% "$IDLEPERCENT"    pausing restic executable"; [[ -z "$resticpid" ]] || kill -STOP $(pidof restic 2>/dev/null) ; } ;
  [[ "$IDLEPERCENT" -ge "$DESIREDIDLEPERCENT" ]] && { [[ "$DEBUGME" = "true" ]] &&  echo "IDLE% "$IDLEPERCENT" continuing restic executable"; [[ -z "$resticpid" ]] || kill -CONT $(pidof restic 2>/dev/null) ; } ;
echo -n ; } ;


[[ "$DEBUGME" = "true" ]] && echo ":pre:func:notify" >&2
## webhook is favoured over telegram
TELEGRAM_CONFIG=false
TGCONFIG_FILE=/dev/null
test -f /etc/telegram-notify.pam.conf    && TGCONFIG_FILE=/etc/telegram-notify.pam.conf
test -f /etc/telegram-notify.backup.conf && TGCONFIG_FILE=/etc/telegram-notify.backup.conf
grep -v ^api-key=$ ${TGCONFIG_FILE} |grep -v -q ^api-key=bot && TELEGRAM_CONFIG=OK
#echo $TELEGRAM_CONFIG
#echo $TGCONFIG_FILE
[[ "$TGCONFIG_FILE" = "/dev/zero" ]] && TELEGRAM_CONFIG=NO
_tg_notify() {
LOCALMSGTXT="$1"
which telegram-notify &>/dev/null && (
#echo TG_NOTIFY_FOUND
test -f /etc/telegram-notify.backup.debug.conf && TGCONFIG_FILE=/etc/telegram-notify.backup.debug.conf

echo ${TELEGRAM_CONFIG} |grep -q ^OK$ && (
 ( echo "$LOCALMSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent  --text - )
     ) )
echo -n ; } ;

[[ "$DEBUGME" = "true" ]] && echo ":pre:func:loadstat" >&2

_debug_loadstatistics() {

MYMSGTXT=$(
            (
            echo "I backup  stage: "${STAGE}' I substage: '$1' I';            
            echo "I cpulimits: allowed: "${CPUMAXPERC}' prcnt (equals '${CPUMAXNUMERIC}' cores) I penalty: '$LOADPENALTYPERCENT' prcnt I' ;
            echo -n uptime: ;
            uptime|sed 's/ up /\n up /g;s/users.\+load/users\nload/g'|grep -e load -e users;
            echo;
            ##echo memory;            #free -m
            awk '/^MemTotal/ { t=$2 } /^MemFree/ { f=$2 } END { printf "memory_percentfree_simple=%.2f\n", ( f / t * 100 ) }' /proc/meminfo; ##available is not readable in some vm versions e.g. openvz
            awk '/^MemTotal/ { t=$2 } /^MemFree/ { f=$2 } /^Buffers/ { b=$2 } /^Cached/ { c=$2 } /^MemAvailable/ { a=$2 } END { printf "memory_percentfree_buffcache=%.2f\n", ((f+b+c)/t*100) }' /proc/meminfo;
            grep -e "[0-9]" /proc/swaps |awk '{print  $1 "=" (-$4/$3*100) }'|sed 's/^/sys:mem:percent:swap:/g;s/\(\/\|\t\)/_/g;s/_\+/_/g';
            echo "sys:_mem-percent_ram="$(echo $(grep -e MemTotal -e MemFree -e Buffers -e Cached /proc/meminfo|sed 's/\([0-9]\+\) kB/\1/g;s/\( \|\t\)//g;'|cut -d: -f2)|awk '{print 100-100*($2+$3+$4)/$1}')
            echo "SYS:MEM:USED::MB: "$( awk "BEGIN {print  "$(echo $(grep -e MemTotal -e MemFree -e Buffers -e Cached /proc/meminfo|sed 's/\([0-9]\+\) kB/\1/g;s/\( \|\t\)//g;'|cut -d: -f2)|awk '{print 100-100*($2+$3+$4)/$1}')" /100 * "$(cat /proc/meminfo |grep MemTotal|cut -d: -f2|sed 's/ \+/ /g'|cut -d" " -f2)" / 1024 }")
            echo "SYS:MEM:TOTAL:MB: "$(($(sed 's/ \+/ /g' /proc/meminfo |grep MemTotal|cut -d" " -f2)/1024))
            echo -n) |sed 's/^/#DBG# : /g' |tr -cd '[:alnum:]\n:_=\-,. #'
          )
          #|sed 's/-/\\\\-/g'
echo "${MYMSGTXT}" >&2
_tg_notify "${MYMSGTXT}"

echo -n ; } ;

### TESTS
[[ "$DEBUGME" = "true" ]] && echo ":pre:exec" >&2

execmissing=0
for executable in jq restic;do
     which "$executable" &>/dev/null || _apt_install_if_missing "$executable"
     which "$executable" &>/dev/null || { echo "MISSING $executable in PATH"; execmissing=1 ; };
  done

if [ "$execmissing" -eq 1 ] ; then exit 1 ;fi


[[ "$DEBUGME" = "true" ]] && echo ":pre:init" >&2


## INIT

startdate=$(date -u +%s)
mylogdest=/tmp/docker2restic_${startdate}.log
##restic json timestamps are in ISO 8601
ZERODATE="1907-01-01T01:01:01.000000000+01:00"


#echo ${RESTIC_AWS_URL}


[[ "$DEBUGME" = "true" ]] && echo ":pre:variables" >&2
##BACKUP system things first


## if minage is not numeric , we set it to 12h == 43200 seconds
case ${RESTIC_MINAGE} in
    ''|*[!0-9]*) export RESTIC_MINAGE=43200 ;;
    #*) echo good ;;
esac

case $RESTIC_MAXUPLOAD in
    ''|*[!0-9]*) export RESTIC_MAXUPLOAD=100000 ;; ## 1 Gbit if not set
    #*) echo good ;;
esac

test -f /etc/restic.exclude.containers.conf || touch  /etc/restic.exclude.containers.conf


export PLACE="system"
export STAGE="system"

export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}

[[ "$DEBUGME" = "true" ]] && echo ":pre:repotarget:"$RESTIC_REPOSITORY >&2

[[ "$DEBUGME" = "true" ]] && echo ":pre:notify-send" >&2
which notify-send &>/dev/null && notify-send "Restic backup started." 2>&1 | tee -a ${mylogdest} >/dev/null

[[ "$DEBUGME" = "true" ]] && echo ":pre:snaplist" >&2
## restic strangely backgrounds since 2021-0x
snapshots=$(echo | restic snapshots --json 2>/dev/null);snap_time_json=$(echo "$snapshots" | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g');
if [ -z "$FAST" ]; then export FORCE_BACKUP=" --force " ;else  export FORCE_BACKUP="";FORCE_BACKUP="" ;fi ;


[[ "$DEBUGME" = "true" ]] && echo ":pre:snapdate $snapshots" >&2
echo $snap_time_json|grep -e ^1907-01 -e ^1970-01 -e ^$ -q && { snapshots=$(echo | restic snapshots --json 2>/dev/null);snap_time_json=$(echo "$snapshots" | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g') ; } ;
[[ -z "$snap_time_json" ]] &&  snap_time_json="$ZERODATE";
[[ -z "$snapshots" ]]      &&  snap_time_json="$ZERODATE";


[[ "$DEBUGME" = "true" ]] && echo ":pre:snapjson:    $snap_time_json"  >&2

snapshot_time=$(date -u -d "$snap_time_json" +%s);
[[ "$DEBUGME" = "true" ]] && echo ":pre:snaptime:    $snapshot_time "  >&2

case $snapshot_time in
    ''|*[!0-9]*) export snapshot_time=0 ;;
    #*) echo good ;;
esac

snapshot_age=$(($(date -u +%s)-$snapshot_time));
[[ "$DEBUGME" = "true" ]] && echo ":pre:snapage:    $snapshot_age"  >&2

echo "::SYSTEM: (last backup $snapshot_age seconds ago, threshold ${RESTIC_MINAGE})" | _logger_sys

_restic_init() {

[[ "$DEBUGME" = "true" ]] && echo ":restic:init    "$(export|grep RESTIC|sed 's/PASSWORD=.\+/PASSWORD=***/g') >&2

  echo | restic init 2>&1 |grep -v "already"|grep -v -e "Please note that knowledge of your" -e "the repository. Losing your password means th" -e "rrecoverably lost" -e "password is correct" | tee -a ${mylogdest} >/dev/null ; } ;

echo $(echo -n "before backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || ( echo "0 snapshots" ; _restic_init ) ) | _logger_sys

snapshots=$(echo | restic snapshots --json 2>/dev/null | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g' ) 


[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "BEFORE:START"


if test "$snapshot_age" -ge "${RESTIC_MINAGE}"  ;then
        echo ":1:SYSTEM:1:BACKING UP.." | _logger_sys
## this commands run unthrottled
        _restic_init
          restic unlock --cleanup-cache 2>&1 | _logger_sys
## now we throttle restic
_sleep_on_high_load ;
        touch /dev/shm/.restic_system_running
        while ( test -e /dev/shm/.restic_system_running ) ;do
        [[ -z "$(pidof restic 2>/dev/null)" ]] && rm /dev/shm/.restic_system_running
        _stop_restic_on_load
            sleep 5
        done &
  #--one-file-system
  #since we backup docker in another stage , no need to stress it here ##
  ## auto-generating exclude file below
        test -e /etc/restic.exclude.conf || ( ( echo "/lib/modules";
                                                echo "/usr/share/man" ;
                                                echo "/root/.cache";
                                                echo '/home/*/.cache' ;
                                                echo /var/log; echo /var/lib/docker/overlay2 ;
                                                echo /dev;
                                                echo /media;
                                                echo /proc;
                                                echo /run;
                                                echo /sys;
                                                echo /mnt/storagespace/docker/volumes/;
                                                echo '/mnt/storagespace*/docker/volumes/';
                                                echo /tmp/;
                                                echo /var/tmp ;
                                                echo /var/lib/docker/volumes/ ;
                                                echo /var/lib/apt/lists;
                                                echo /var/cache/ ;
                                                echo '/storage_global/*/_docker_internal_storage/' ;
                                                echo '/storage_global/*/_docker_volumes')  > /etc/restic.exclude.conf )

[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics ":run:backup"
HAVE_INCLUDE=false
test -e /etc/restic.include.conf && HAVE_INCLUDE=true
[[ "$DEBUGME" = "true" ]] && echo HAVE_INCLUDE is $HAVE_INCLUDE
sys_backed_up=no;
  [[ "$sys_backed_up" = "no" ]] && [[ "${HAVE_INCLUDE}" = "true" ]]  && [[ "${HAVE_CPULIMIT}" = "true" ]]  || { sys_backed_up=yes ; ( [[ "${DEBUGME}" = "true" ]] &&  echo "NO CPULIMIT FOUND1"                 | _logger_sys ; export GOGC=20; export GOMAXPROCS=1 ;GOGC=20 GOMAXPROCS=1  nice -n 19 ionice -c2 -n7                                                        restic ${NO_CACHE} backup  ${FORCE_BACKUP} --no-scan --verbose --limit-upload $RESTIC_MAXUPLOAD  --files-from=/etc/restic.include.conf  --exclude-file=/etc/restic.exclude.conf   2>&1   |grep -v -e "password is correct"   2>&1 | _logger_sys )  ; } ;
  [[ "$sys_backed_up" = "no" ]] && [[ "${HAVE_INCLUDE}" = "true" ]]  && [[ "${HAVE_CPULIMIT}" = "true" ]]  && { sys_backed_up=yes ; ( [[ "${DEBUGME}" = "true" ]] &&     echo "CPULIMIT FOUND1"                 | _logger_sys ; export GOGC=20; export GOMAXPROCS=2 ;GOGC=20 GOMAXPROCS=2  nice -n 19 ionice -c2 -n7  cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic ${NO_CACHE} backup  ${FORCE_BACKUP} --no-scan --verbose --limit-upload $RESTIC_MAXUPLOAD  --files-from=/etc/restic.include.conf  --exclude-file=/etc/restic.exclude.conf   2>&1   |grep -v -e "password is correct"   2>&1 | _logger_sys )  ; } ;
  [[ "$sys_backed_up" = "no" ]] && [[  "${HAVE_INCLUDE}" = "false" ]]  && [[ "${HAVE_CPULIMIT}" = "true" ]]  || { sys_backed_up=yes ; ( [[ "${DEBUGME}" = "true" ]] &&  echo "NO CPULIMIT FOUND2,NO INCLUDE FILE" | _logger_sys ; export GOGC=20; export GOMAXPROCS=1 ;GOGC=20 GOMAXPROCS=1  nice -n 19 ionice -c2 -n7                                                        restic ${NO_CACHE} backup  ${FORCE_BACKUP} --no-scan --verbose --limit-upload $RESTIC_MAXUPLOAD                                        --exclude-file=/etc/restic.exclude.conf / 2>&1 |grep -v -e "password is correct"  2>&1 | _logger_sys )                                          ; } ;
  [[ "$sys_backed_up" = "no" ]] && [[  "${HAVE_INCLUDE}" = "false" ]]  && [[ "${HAVE_CPULIMIT}" = "true" ]]  && { sys_backed_up=yes ; ( [[ "${DEBUGME}" = "true" ]] &&     echo "CPULIMIT FOUND2,NO INCLUDE FILE" | _logger_sys ; export GOGC=20; export GOMAXPROCS=2 ;GOGC=20 GOMAXPROCS=2  nice -n 19 ionice -c2 -n7  cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic ${NO_CACHE} backup  ${FORCE_BACKUP} --no-scan --verbose --limit-upload $RESTIC_MAXUPLOAD                                        --exclude-file=/etc/restic.exclude.conf / 2>&1 |grep -v -e "password is correct"  2>&1 | _logger_sys )                                          ; } ;
else
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics ":TOORECENT:donotrun:BACKUP"

fi


_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER:SYSBACKUP"
## re-adjust penalty and cpulimits to current load
LOADPENALTYPERCENT=$(_calculate_loadpenalty)
############################DEFAULTMAXPERC ...
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
######################################################

echo ":1:SYSTEM:2:DELETE_and_PRUNE::" 2>&1 | _logger_sys
echo ":1:SYSTEM:2.1:DELETE" 2>&1 | _logger_sys
(sleep 6; _renice_restic 5 ) &
test -e /etc/restic.include.conf || [[ "${HAVE_CPULIMIT}" = "true" ]]  || (  export GOGC=20; export GOMAXPROCS=1 ;GOGC=20 GOMAXPROCS=1  nice -n 19 ionice -c2 -n7                                          restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys )
test -e /etc/restic.include.conf || [[ "${HAVE_CPULIMIT}" = "true" ]]  && (  export GOGC=20; export GOMAXPROCS=2 ;GOGC=20 GOMAXPROCS=2  nice -n 19 ionice -c2 -n7  cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys )

echo ":1:SYSTEM:2.2:PRUNE" 2>&1 | _logger_sys
(sleep 6; _renice_restic 5 ) &
forgetlist="";
restic unlock;

##forget old ones since restic policy fails
rm /dev/shm/.restic_forget_list;restic unlock
restic snapshots|grep "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]"|sed 's/ \+/ /g'|cut -d" " -f1-3|while read line;do id=${line/ *};
shotdate=$(echo "$line"|cut -d" " -f2,3) ;
shotepoch=$(date -d "$shotdate" +%s) ;
shotagesec=$(($(date -u +%s )-$shotepoch));
echo -n "SNAP: $shotdate ( $shotagesec ) "| _logger_sys;
[[ $shotagesec -ge $((86400*${KEEPDAYS}))  ]] && { echo -n "forget $id |" 2>&1 | _logger_sys;forgetlist="$forgetlist $id" ;  } ;
[[ $(echo $forgetlist|wc -w ) -ge 23 ]] && { restic forget $forgetlist 2>&1 |sed 's/$/|/g' |tr -d '\n'  | _logger_sys ;forgetlist="" ; }  ;
echo "$forgetlist" > /dev/shm/.restic_forget_list
echo;
done
forgetlist=$(test -e /dev/shm/.restic_forget_list && cat /dev/shm/.restic_forget_list)
echo $forgetlist|wc -w |grep -q ^0$ ||  { restic forget $forgetlist 2>&1 |sed 's/$/|/g' |tr -d '\n'  | _logger_sys ;forgetlist="" ; }
## end forget
  [[ "${HAVE_CPULIMIT}" = "true" ]]  || GOGC=20 GOMAXPROCS=1 nice -n 19 ionice -c2 -n7                                                       restic ${NO_CACHE} prune  2>&1  | _stdout_delete_filter | _logger_sys
  [[ "${HAVE_CPULIMIT}" = "true" ]]  && GOGC=20 GOMAXPROCS=2 nice -n 19 ionice -c2 -n7 cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic ${NO_CACHE} prune  2>&1  | _stdout_delete_filter | _logger_sys

echo $(echo -n "after backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_sys
echo "///SYSTEM DONE" | _logger_sys
## kill the sys throttle
test -e /dev/shm/.restic_system_running && rm /dev/shm/.restic_system_running
sleep 2

## cool down
_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER_SYS_PRUNE"
## re-adjust penalty and cpulimits to current load
LOADPENALTYPERCENT=$(_calculate_loadpenalty)
############################DEFAULTMAXPERC ...
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
######################################################

## free ram ( dangerous NFS-backend systems will have their file-cache filled heavily at this point  )
_dropcaches

# safety if the watchdog is still running
test -e /dev/shm/.restic_system_running && rm /dev/shm/.restic_system_running

### start docker stage backup
echo "::DOCKER:" | _logger_sys





which docker >/dev/null && docker ps --format '{{.Names}}' -q|grep -v -e "^nginx-gen$"  |while read a;do
  test -e  /dev/shm/.restic_volumes_running && rm /dev/shm/.restic_volumes_running; #cleanup stale
  sleep 15;                            ## so the other watchers end
  touch /dev/shm/.restic_volumes_running
  while ( test -e /dev/shm/.restic_volumes_running ) ;do
    test -e /dev/shm/.restic_volumes_running_unthrottled || _stop_restic_on_load
    [[ -z "$(pidof restic 2>/dev/null)" ]] && rm /dev/shm/.restic_volumes_running
      sleep 7
  done &

  _sleep_on_high_load ;
  _dropcaches &
  sleep 1
  #touch /dev/shm/.restic_volumes_running_unthrottled
                export PLACE="docker/volumes/$a"
                ## REST_SERVER EXPECTS FLAT FORM( HAS ONE ROOT DIR)
                echo "${RESTIC_AWS_URL}" |grep -q -e "http://" -e "https://" && export PLACE="docker_volumes_$a"
                ## BACKUPS INTO A SINGLE REPO 
                [[ "${SINGLE_REPO}" = "true" ]]      && export PLACE="docker_volumes"
                [[ "${DOCKER_TO_SYSTEM}" = "true" ]] && export PLACE="system"
                export STAGE="DOCKER-VOL-${a}"
                ####REMOVE DUPLICATE SLASHES (same as tr -s ' ' a.k.a. squeeze repeat)
                export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
                (sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 10;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 23 ;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 42;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic ) &
                snapshots=$(echo | restic snapshots --json 2>/dev/null | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g');snap_time_json=$(echo "$snapshots");
                [[ "$DEBUGME" = "true" ]] && echo "$snapshots" |jq .
                if [ -z "$snap_time_json" ]; then snap_time_json="$ZERODATE";fi ;
                snapshot_time=$(date -u -d "$snap_time_json" +%s);
                case $snapshot_time in
                    ''|*[!0-9]*) export snapshot_time=0 ;; #*) echo good ;;
                esac
    ##${RESTIC_MINAGE}
                snapshot_age=$(($(date -u +%s)-$snapshot_time));

##put container-names you do not  want to backup in /etc/restic.exclude.containers.conf ( e.g. the _redirwww containers)

## exclude containers that are cached OR have their configs already in docker-compose dir
                skipthis=0;
                grep -q "^$a$" /etc/restic.exclude.containers.conf && skipthis=1;
                if [[ $a =~ "apt-cache" ]] ;     then skipthis=1;fi
                if [[ $a =~ "redis_" ]] ;        then skipthis=1;fi
                if [[ $a =~ "redis"$ ]] ;        then skipthis=1;fi
                if [[ $a =~ "_redirect"$ ]] ;    then skipthis=1;fi
                if [[ $a =~ "memcache_" ]] ;     then skipthis=1;fi
                if [[ $a =~ "memcached" ]] ;     then skipthis=1;fi
                if [[ $a =~ "-cron"$ ]] ;        then skipthis=1;fi
                if [[ $a =~ ".cron"$ ]] ;        then skipthis=1;fi
                if [[ $a =~ "sftp"$ ]] ;         then skipthis=1;fi
                #self filter
                if [[ $a =~ "docker2restic" ]] ; then skipthis=1;fi
                #do not snapshot too early
                if [ "$snapshot_age" -lt "${RESTIC_MINAGE}"  ];then skipthis=1;fi

                if [ "$skipthis" -eq 0 ];then
                echo "==>CONTAINER: "$a" (last backup $snapshot_age seconds ago, threshold ${RESTIC_MINAGE})" | _logger_volume
                ###### _volume_json() { docker inspect -f '{{json .Mounts }}' "$a"|jq -rcM .[] - |grep -v '"Source":"/root/.ssh' ; } ;
                _volume_json() { docker inspect -f '{{json .Mounts }}' "$a"|jq -rcM .[] - ; } ;
                backing_mounts=$( _volume_json|jq .Destination ) ;
                test -f /tmp/docker2restic_${startdate}.mounts.list && rm /tmp/docker2restic_${startdate}.mounts.list
                ###### we prevent the /root/.ssh folder , either specify it in system backup settings ,or (BETTER) do not put privkeys where they do not belong, use ssh -A

                echo "$backing_mounts" |sed 's/"//g'|grep -v -e "/root/.ssh" -e "/var/log" -e "^/proc" -e "^/sys" -e "^/dev" > /tmp/docker2restic_${startdate}.mounts.list
                if [[ -z "$backing_mounts" ]] ; then  skipthis=1;fi
                [[ "$DEBUGME" = "true" ]] && echo $backing_mounts >&2
                [[ "$DEBUGME" = "true" ]] && echo removing old container >&2
                [[ -z "$backing_mounts" ]] ||
                ( docker ps -a 2>&1 |grep docker2restic && docker stop docker2restic 2>&1 )| sed 's/^/PREREMOVE:/g'|_logger_volume
                sleep 2
                [[ -z "$backing_mounts" ]] || ( docker ps -a 2>&1 |grep docker2restic && docker rm   docker2restic 2>&1 )| sed 's/^/PREREMOVE:/g'|_logger_volume
                [[ -z "$backing_mounts" ]] || { echo -n;
                echo -n ":2:CONTAINER:1:BACKING UP VOLUMES("${PLACE}") >> MOUNTS :" $backing_mounts 2>&1 | _logger_volume
#                docker run --hostname=restic-$(hostname) --rm --name restic  -v ${HOME}/.ssh:/root/.ssh -v ${HOME}/.ssh:/root/.ssh --volumes-from "$a":ro -e RESTIC_REPOSITORY  -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID restic/echo | restic init
                echo | restic init 2>&1 |grep -v "already initialized" |grep -v -e "Please note that knowledge of your" -e "the repository. Losing your password means th" -e "rrecoverably lost" -e "password is correct" 2>&1 | _logger_volume
                restic unlock --cleanup-cache 2>&1 | _logger_volume
                #rm /dev/shm/.restic_volumes_running_unthrottled
echo $(echo -n "before backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_volume
#export
#echo                      /bin/bash -c 'docker run --hostname=restic-$(hostname) --cpus 2.0 --rm --name docker2restic -d --tmpfs /root/.cache/restic:rw,noexec,nosuid,size=131072k -v ${HOME}/.ssh/known_hosts:/root/.ssh/known_hosts --volumes-from "'$a'":ro -e RESTIC_REPOSITORY  -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID restic/restic backup '${backing_mounts}' ' 2>&1 #  |grep -v -e "Please note that knowledge of your" -e "the repository. Losing your password means th" -e "rrecoverably lost" -e "password is correct"

##                echo forking 'docker run --hostname=restic-$(hostname) --blkio-weight 666 --cpus '${CPUMAXNUMERIC}' --rm --hostname docker2restic --name docker2restic -v /root/.cache/restic:/root/.cache/restic  --volumes-from "'$a'":ro --mount type=bind,source='/tmp/docker2restic_${startdate}.mounts.list',target=/include.conf -e RESTIC_REPOSITORY -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID  thefoundation/restic-fantastic nice -n 19 ionice -c2 -n7 cpulimit --limit '${CPUMAXPERC}' -f -m  restic --limit-upload '${RESTIC_MAXUPLOAD}' backup --verbose --files-from=/include.conf  '${FORCE_BACKUP} 2>&1 | _logger_volume &
##                /bin/bash -c 'docker run --hostname=restic-$(hostname) --blkio-weight 666 --cpus '${CPUMAXNUMERIC}' --rm --hostname docker2restic --name docker2restic -v /root/.cache/restic:/root/.cache/restic  --volumes-from "'$a'":ro --mount type=bind,source='/tmp/docker2restic_${startdate}.mounts.list',target=/include.conf -e RESTIC_REPOSITORY -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID  thefoundation/restic-fantastic nice -n 19 ionice -c2 -n7 cpulimit --limit '${CPUMAXPERC}' -f -m  restic  --limit-upload '${RESTIC_MAXUPLOAD}' backup --verbose --files-from=/include.conf  '${FORCE_BACKUP} 2>&1 | _logger_volume &
                touch /dev/shm/.restic_active

                RESTICPID=$!
                echo RESTICPID=$RESTICPID
                [[ "$DEBUGMEHARDER" = "true" ]] && echo "cmdline of docker run: file: /proc/$RESTICPID/cmdline .. content: "$(cat /proc/$RESTICPID/cmdline)
#                while( test -e /dev/shm/.restic_active && test -e /proc/$RESTICPID && grep -q docker2restic /proc/$RESTICPID/cmdline 2>/dev/null) ;do
                while( test -e /dev/shm/.restic_active  ) ;do
                
                  LOADSHORTPERCENT=$(   awk 'BEGIN { print '$(cat /proc/loadavg |cut -d" " -f1)'*100 }' );
                  CPUAVAILABLEPERCENT=$(awk 'BEGIN { print '$(nproc)'*100 }');
                  IDLEPERCENT=$(        awk "BEGIN { print $CPUAVAILABLEPERCENT - $LOADSHORTPERCENT }");
                  IDLEPERCENT=$(echo "${IDLEPERCENT}"|cut -d"." -f1);
                  [[ "$(nproc)" = "1" ]] || DESIREDIDLEPERCENT=$((($CPUAVAILABLEPERCENT/$(($(nproc)+1)))+50))
                  [[ "$(nproc)" = "1" ]]             && DESIREDIDLEPERCENT=50
                  [[ "$IDLEPERCENT" -le 100 ]]       && DESIREDIDLEPERCENT=50
                  [[ "$DESIREDIDLEPERCENT" -le 50 ]] && DESIREDIDLEPERCENT=50
                  [[ "$IDLEPERCENT" -le "$DESIREDIDLEPERCENT" ]] && { 
                    [[ "$DEBUGME" = "true" ]] &&  echo "IDLE% "$IDLEPERCENT"    pausing restic executable"; 
                    [[ -z "$(pidof restic)" ]] || killall -STOP $(pidof restic) 
                    #docker ps  -a |grep docker2restic|grep -q -i pause || docker   pause docker2restic 2>&1 |grep -v "already paused" 
                    echo -n ; } ;
                  [[ "$IDLEPERCENT" -ge "$DESIREDIDLEPERCENT" ]] && { 
                    [[ "$DEBUGME" = "true" ]] &&  echo "IDLE% "$IDLEPERCENT" continuing restic executable";
                    #docker ps  -a |grep docker2restic|grep -q -i pause && docker unpause docker2restic 2>&1 |grep -v ^docker2restic|grep -v "is not paused"    
                    [[ -z "$(pidof restic)" ]] || killall -CONT $(pidof restic)
                    echo -n ; } ;
                  IDLECPUS=$(awk 'BEGIN {print '$IDLEPERCENT'/100}')
                  [[ "$DEBUGME" = "true" ]] && echo -n "DOCKER_UPDATE_CPUS $IDLECPUS $IDLEPERCENT" >&2
                  [[ "$IDLEPERCENT" -le 0 ]] && IDLEPERCENT=42
                  [[ "$(nproc)" = "1" ]] && IDLECPUS=1
                    timeout 30 docker update  --cpus "$IDLECPUS" docker2restic 2>&1 |grep -v ^docker2restic$
                    _sleep_on_high_load
                sleep 10
               done &
#                wait



                CPULIMITCONTAINER=$CPUMAXPERC
                ### alpine cpulimit has only 400% max limit
                [[ ${CPUMAXPERC} -gt  400 ]] && CPULIMITCONTAINER=400
                (sleep 7 ;[[ "$DEBUGMEHARDER" = "true" ]] && docker ps  |grep  docker2restic ) &
                (sleep 13 ;docker ps  |grep -q docker2restic |grep -i "paused" || { docker ps  |grep -q docker2restic && timeout 30 docker exec docker2restic cpulimit --limit=${CPULIMITCONTAINER} -e $(docker exec docker2restic bash -c "which restic")  2>&1  |grep -v -e "^Process.*dead.*" -e "^Process.*detected"   ; } ; ) &
                (sleep 23 ;docker ps  |grep -q docker2restic |grep -i "paused" || { INTERNALPID=$(timeout 30 docker exec docker2restic bash -c "pidof restic 2>/dev/null"); [[ -z "$INTERNALPID" ]] || (timeout 30 docker ps  |grep -q docker2restic) && timeout 30 docker exec docker2restic ionice -n 7  -p $INTERNALPID 2>&1 |grep -v -e "old prio" -e detected$                                                 ; } ;  ) &
                (sleep 42 ;docker ps  |grep -q docker2restic |grep -i "paused" || { INTERNALPID=$(timeout 30 docker exec docker2restic bash -c "pidof restic 2>/dev/null"); [[ -z "$INTERNALPID" ]] || (timeout 30 docker ps  |grep -q docker2restic) && timeout 30 docker exec docker2restic renice -n 7  -p $INTERNALPID 2>&1 |grep -v -e "old prio" -e detected$                                                 ; } ;  ) &
                echo docker run --hostname=restic-$(hostname) --hostname docker2restic --name docker2restic --blkio-weight 666 --cpus ${CPUMAXNUMERIC} --rm --name docker2restic -v /root/.config/rclone:/root/.config/rclone:ro -v /root/.cache/restic:/root/.cache/restic  --volumes-from ${a}:ro --mount type=bind,source=/tmp/docker2restic_${startdate}.mounts.list,target=/include.conf -e RESTIC_REPOSITORY -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID  thefoundation/restic-fantastic restic ${NO_CACHE} backup  ${FORCE_BACKUP} --no-scan --verbose --hostname "$a"   --limit-upload $RESTIC_MAXUPLOAD --exclude /root/.config/rclone --exclude /root/.ssh --files-from=/include.conf  &
                     docker run --hostname=restic-$(hostname) --hostname docker2restic --name docker2restic --blkio-weight 666 --cpus ${CPUMAXNUMERIC} --rm --name docker2restic -v /root/.config/rclone:/root/.config/rclone:ro -v /root/.cache/restic:/root/.cache/restic  --volumes-from ${a}:ro --mount type=bind,source=/tmp/docker2restic_${startdate}.mounts.list,target=/include.conf -e RESTIC_REPOSITORY -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID  thefoundation/restic-fantastic restic ${NO_CACHE} backup  ${FORCE_BACKUP} --no-scan --verbose --hostname "$a"   --limit-upload $RESTIC_MAXUPLOAD --exclude /root/.config/rclone --exclude /root/.ssh --files-from=/include.conf ;

                sleep 10;rm /dev/shm/.restic_active
                _sleep_on_high_load ;
                _dropcaches
                [[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "BEFORE:VOLUMEPRUNE"
                #/bin/bash -c 'docker run --hostname=restic-$(hostname) --cpus 2.0 --rm --hostname docker2restic --name docker2restic -v ${HOME}/.ssh/known_hosts:/root/.ssh/known_hosts --volumes-from "'$a'":ro  -e RESTIC_REPOSITORY -e RESTIC_PASSWORD -e AWS_SECRET_ACCESS_KEY -e AWS_ACCESS_KEY_ID  restic/restic --limit-upload '${RESTIC_MAXUPLOAD}' backup '${backing_mounts}' '   2>&1 | _logger_volume

                docker stop docker2restic 2>&1 |grep -v "Error: No such container: " |grep -v docker2restic 2>&1 | sed 's/^/POSTREMOVE:/g' | _logger_volume
                sleep 2
                docker rm docker2restic   2>&1 |grep -v "Error: No such container: " |grep -v docker2restic 2>&1 | sed 's/^/POSTREMOVE:/g' | _logger_volume
                echo ":1:CONTAINER:2:DELETE_and_PRUNE::" 2>&1 | _logger_volume

                ##forget old ones since restic policy fails
                rm /dev/shm/.restic_forget_list;restic unlock
                restic snapshots|grep "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]"|sed 's/ \+/ /g'|cut -d" " -f1-3|while read line;do id=${line/ *};
                shotdate=$(echo "$line"|cut -d" " -f2,3) ;
                shotepoch=$(date -d "$shotdate" +%s) ;
                shotagesec=$(($(date -u +%s )-$shotepoch));
                echo -n "SNAP: $shotdate ( $shotagesec ) " 2>&1 | _logger_volume
                [[ $shotagesec -ge $((86400*${KEEPDAYS}))  ]] && { echo -n "forget $id |" 2>&1 | _logger_volume ;forgetlist="$forgetlist $id" ;  } ;
                [[ $(echo $forgetlist|wc -w ) -ge 23 ]] && { restic forget $forgetlist 2>&1 |sed 's/$/|/g' |tr -d '\n'  | _logger_volume ;forgetlist="" ; }  ;
                echo "$forgetlist" > /dev/shm/.restic_forget_list
                echo;
                done
                forgetlist=$(test -e /dev/shm/.restic_forget_list && cat /dev/shm/.restic_forget_list)
                echo $forgetlist|wc -w |grep -q ^0$ ||  { restic forget $forgetlist 2>&1 |sed 's/$/|/g' |tr -d '\n'     | _logger_volume;forgetlist="" ; }
                ## end forget

                (sleep 6; _renice_restic 5 ) &
                [[ "${HAVE_CPULIMIT}" = "true" ]]  && GOGC=20 GOMAXPROCS=2 nice -n 19 ionice -c2 -n7 cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} --  restic forget --keep-within ${KEEPDAYS}"d" 2>&1 |grep -v "ID Time" |tac | _stdout_delete_filter   | _logger_volume
                [[ "${HAVE_CPULIMIT}" = "true" ]]  || GOGC=20 GOMAXPROCS=1 nice -n 19 ionice -c2 -n7                                          restic forget --keep-within ${KEEPDAYS}"d" 2>&1 |grep -v "ID Time" |tac | _stdout_delete_filter   | _logger_volume
                (sleep 6; _renice_restic 5 ) &
                [[ "${HAVE_CPULIMIT}" = "true" ]]  && GOGC=20 GOMAXPROCS=2 nice -n 19 ionice -c2 -n7 cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} --   restic ${NO_CACHE} prune 12>&1                             |grep -v "ID Time" |tac | _stdout_delete_filter   | _logger_volume
                [[ "${HAVE_CPULIMIT}" = "true" ]]  || GOGC=20 GOMAXPROCS=1 nice -n 19 ionice -c2 -n7                                                          restic ${NO_CACHE} prune 12>&1                             |grep -v "ID Time" |tac | _stdout_delete_filter   | _logger_volume
                echo $(echo -n "after backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_volume
            echo -n ; } ;
        fi
        ##### END VOLUMES IF SKIPTHIS

test -f /tmp/docker2restic_${startdate}.mounts.list && rm /tmp/docker2restic_${startdate}.mounts.list

[[ "${DEBUGME}" = "true" ]] && _debug_loadstatistics "ENDVOLUME"

### re-adjust penalty and cpulimits to current load
LOADPENALTYPERCENT=$(_calculate_loadpenalty)
############################DEFAULTMAXPERC
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
######################################################

################################################################################################################
                #detect database containers, in addition to a file backup , we will dump them as well
                mysqlcont=0;pgsqlcont=0;
                _volume_json() { docker inspect -f '{{json .Mounts }}' "$a"|jq -rcM .[] - ; } ;
                backing_mounts=$( _volume_json|jq .Destination ) ;
                [[ "$DEBUGME" = "true" ]] && echo $backing_mounts >&2

                echo "$backing_mounts" |grep -q "/var/lib/mysql"   && mysqlcont=1
                echo "$backing_mounts" |grep -q "/var/lib/postgre" && pgsqlcont=1
                test -f /tmp/docker2restic_${startdate}.mounts.list && rm /tmp/docker2restic_${startdate}.mounts.list
                echo "$backing_mounts" |sed 's/"//g'> /tmp/docker2restic_${startdate}.mounts.list
                [[ "$DEBUGME" = "true" ]] && echo $backing_mounts >&2
                [[ "$DEBUGME" = "true" ]] && echo removing old container >&2
            [[ -z "$backing_mounts" ]] || { echo -n;
                if [[ $a =~ "memcached" ]] ; then mysqlcont=0; pgsqlcont=0;fi
                if [[ $a =~ "redis" ]] ; then mysqlcont=0; pgsqlcont=0;fi
                timeout 30 docker exec -t "$a" which postgres|tr -cd '[:alnum:]/'|sed 's/ .\+//g'|grep -q "/postgres$" && pgsqlcont=1
                timeout 30 docker exec -t "$a" which mysqld|tr -cd '[:alnum:]/'|sed 's/ .\+//g'|grep -q "/mysqld$" && mysqlcont=1

                test -e /etc/restic.exclude.containerdatabases.conf && grep -q "^$a" /etc/restic.exclude.containerdatabases.conf && mysqlcont=0;
                test -e /etc/restic.exclude.containerdatabases.conf && grep -q "^$a" /etc/restic.exclude.containerdatabases.conf && pgsqlcont=0;
                export STAGE=DOCKER-DB-PGSQL-${a}
		        export PLACE=docker/database/${a} ;
                ## REST_SERVER EXPECTS FLAT FORM( HAS ONE ROOT DIR)
                echo "${RESTIC_AWS_URL}" |grep -q -e "http://" -e "https://" && export PLACE="docker_database_$a"
                ## BACKUPS INTO A SINGLE REPO 
                [[ "${SINGLE_REPO}" = "true" ]]      && export PLACE="docker-pgsql"
                [[ "${DOCKER_TO_SYSTEM}" = "true" ]] && export PLACE="system-pgsql"
                
                # tricks from here https://forum.restic.net/t/recipe-to-snapshot-postgres-container/1707
                ##check psql
                if [ "$pgsqlcont" -eq 1 ] ; then
                                      export STAGE=DOCKER-DB-PGSQL-${a}
                                      echo '===>PGSQL: '${PLACE} | _logger_pgsql ;
                                      export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
                                      echo | restic init 2>&1 |grep -v -e "already initialized" -e "Please note that knowledge of your" -e "the repository. Losing your password means th" -e "rrecoverably lost" -e "password is correct"  2>&1 | _logger_pgsql ;
                                                   restic unlock --cleanup-cache 2>&1 | _logger_pgsql ;
_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "BEFORE_PGSQL_DUMP"
                fi
                if [ "$pgsqlcont" -eq 1 ] ; then
                                        (sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic ) &
                                        snapshots=$(echo | restic snapshots --json 2>/dev/null | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g');snap_time_json=$(echo "$snapshots");
                                        if [ -z "$snap_time_json" ]; then snap_time_json="$ZERODATE";fi ;
                                        snapshot_time=$(date -u -d "$snap_time_json" +%s);
                                        case $snapshot_time in
                                            ''|*[!0-9]*) export snapshot_time=0 ;;
                                            #*) echo good ;;
                                        esac
                                        snapshot_age=$(($(date -u +%s)-$snapshot_time));
                                        if test "$snapshot_age" -ge "${RESTIC_MINAGE}"  ;then
                                        echo "==>PGSQL: "$a" (last backup $snapshot_age seconds ago, threshold ${RESTIC_MINAGE})" | _logger_pgsql
                                        echo $(echo -n "before backup:"; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" )| _logger_pgsql
                                        (sleep 6; _renice_restic 5 ) &
                                                timeout 7200 docker exec  "$a" bash -c 'pg_dumpall -U$(printenv POSTGRES_USER) || su -s /bin/bash -c pg_dumpall postgres' | gzip --rsyncable | restic backup --hostname "docker_database_$a" --stdin --stdin-filename postgres.sql.gz   2>&1 |tr -d '\r' |grep -v ^$|while  read line;do 
                                                       echo "$line" |wc -c |grep -q -v ^0 && echo  $(date -u "+%F %T")"@"${PLACE}":"${line} |tee -a  /var/log/docker2restic.pgsql.log | tee -a ${mylogdest} >/dev/null ;
                                                       done
                                        fi
_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER_PGSQL_DUMP"

                ##forget old ones since restic policy fails
                rm /dev/shm/.restic_forget_list;restic unlock
                restic snapshots|grep "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]"|sed 's/ \+/ /g'|cut -d" " -f1-3|while read line;do id=${line/ *};
                shotdate=$(echo "$line"|cut -d" " -f2,3) ;
                shotepoch=$(date -d "$shotdate" +%s) ;
                shotagesec=$(($(date -u +%s )-$shotepoch));
                echo -n "SNAP: $shotdate ( $shotagesec ) " | _logger_pgsql;
                [[ $shotagesec -ge $((86400*${KEEPDAYS}))  ]] && { echo -n "forget $id |" | _logger_pgsql ;forgetlist="$forgetlist $id" ;  } ;
                [[ $(echo $forgetlist|wc -w ) -ge 23 ]] && { restic forget $forgetlist 2>&1 |sed 's/$/|/g' |tr -d '\n' | _logger_pgsql ;forgetlist="" ; }  ;
                echo "$forgetlist" > /dev/shm/.restic_forget_list
                echo;
                done
                forgetlist=$(test -e /dev/shm/.restic_forget_list && cat /dev/shm/.restic_forget_list)
                echo $forgetlist|wc -w |grep -q ^0$ ||  { restic forget $forgetlist  2>&1 |sed 's/$/|/g' |tr -d '\n' | _logger_pgsql ;forgetlist="" ; }
                ## end forget
                (sleep 6; _renice_restic 5 ) & restic forget --keep-within ${KEEPDAYS}"d" 2>&1  | _stdout_delete_filter  | _logger_pgsql
                (sleep 6; _renice_restic 5 ) & restic ${NO_CACHE} prune 12>&1                              | _stdout_delete_filter  | _logger_pgsql
                echo $(echo -n "after backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_pgsql
                echo '////PGSQL' | _logger_pgsql
                fi

                ### END PGSQL IF
##########################################################################################
                export STAGE=DOCKER-DB-MYSQL-${a}
                ##check mysql
                if [ "$mysqlcont" -eq 1 ] ; then echo '===> MYSQL: '${PLACE} | _logger_mysql  ;
                                        export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
                                        echo | restic init 2>&1 |grep -v -e "already initialized" -e "Please note that knowledge of your" -e "the repository. Losing your password means th" -e "rrecoverably lost" -e "password is correct" 2>&1 | _logger_mysql ;
                                        restic unlock --cleanup-cache 2>&1 | _logger_mysql ;

                    fi
                _sleep_on_high_load ;
                if [ "$mysqlcont" -eq 1 ] ;then
                                        (sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic;sleep 2;ps -a|grep -q " restic " && ps -a|grep -q " restic " && ps -a|grep -q " restic " && killall -CONT restic ) &
                                        snapshots=$(echo | restic snapshots --json 2>/dev/null | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g');snap_time_json=$(echo "$snapshots");
                                        if [ -z "$snap_time_json" ]; then snap_time_json="$ZERODATE";fi ;
                                        echo $(echo -n "before backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_mysql
                                        snapshot_time=$(date -u -d "$snap_time_json" +%s);
                                        case $snapshot_time in
                                            ''|*[!0-9]*) export snapshot_time=0 ;;
                                            #*) echo good ;;
                                        esac
                                        snapshot_age=$(($(date -u +%s)-$snapshot_time));
                                        if test "$snapshot_age" -ge "${RESTIC_MINAGE}"  ;then
                                          (sleep 6; _renice_restic 5 ) &
                                            echo "==>MYSQL: "$a" (last backup $snapshot_age seconds ago, threshold ${RESTIC_MINAGE})" | _logger_mysql
                                            timeout 3600 docker exec  "$a" sh -c 'which mysqldump |grep -q mysqldump || ( which apk >/dev/null && apk add mariadb-client >/dev/null ;  which apt-get >/dev/null && apt-get update >/dev/null  || true &&  DEBIAN_FRONTEND=noninteractive apt-get install -y mariadb-client &&  DEBIAN_FRONTEND=noninteractive apt-get -y clean;find /var/lib/apt/lists/ -type f -delete 2>/dev/null )' 2>&1 | _logger_mysql
                                            timeout 3600 docker exec  "$a" sh -c 'ok=no ; mysqldump --single-transaction --all-databases 2>/dev/shm/restic.inner.mysqdump.err && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -u root -p$(printenv MARIADB_ROOT_PASSWORD) --single-transaction --all-databases 2>dev/shm/restic.inner.mysqdump.err && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -u root -p$(printenv MYSQL_ROOT_PASSWORD) --single-transaction --all-databases 2>dev/shm/restic.inner.mysqdump.err && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -u $(printenv MARIADB_USERNAME) -p"$(printenv MARIADB_PASSWORD)" --single-transaction --all-databases 2>dev/shm/restic.inner.mysqdump.err && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -u $(printenv MYSQL_USERNAME) -p"$(printenv MYSQL_PASSWORD)" "$(printenv MYSQL_DATABASE)" --single-transaction --all-databases 2>dev/shm/restic.inner.mysqdump.err && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -h ${MYSQL_HOST} -u $(printenv MYSQL_USERNAME) -p"$(printenv MYSQL_PASSWORD)" "$(printenv MYSQL_DATABASE)" --single-transaction --all-databases 2>dev/shm/restic.inner.mysqdump.err && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -h ${MARIADB_HOST} -u $(printenv MARIADB_USERNAME) -p"$(printenv MARIADB_PASSWORD)" "$(printenv MARIADB_DATABASE)" --single-transaction --all-databases && ok="yes"; echo "$ok"|grep -q ^no$ && mysqldump  -h ${DB_HOST} -u $(printenv DB_USER||printenv DB_USERNAME) -p"$(printenv DB_PASSWORD || printenv DB_PASS )" "$(printenv DB_DATABASE)" --single-transaction --all-databases' | gzip --rsyncable | restic backup --hostname "docker_database_$a" --stdin --stdin-filename mysql.sql.gz 2>&1 | _logger_mysql
                    fi
                (sleep 6; _renice_restic 5 ) &
                #forget even if we did not save
                (sleep 6; _renice_restic 5 ) &  restic forget --keep-within ${KEEPDAYS}"d" 2>&1  | _stdout_delete_filter  | _logger_mysql
                (sleep 6; _renice_restic 5 ) &  restic ${NO_CACHE} prune 12>&1                              | _stdout_delete_filter  | _logger_mysql
                echo $(echo -n "after backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_mysql
                echo '////MYSQL' | _logger_mysql
                fi
                ### END MYSQL IF
           echo -n ; } ;

        done

 test -e /dev/shm/.restic_volumes_running && rm /dev/shm/.restic_volumes_running

_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER:BACKUP"
## re-adjust penalty and cpulimits to current load
LOADPENALTYPERCENT=$(_calculate_loadpenalty)
############################DEFAULTMAXPERC ...
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
######################################################


## system mysql

ls -1 /var/run/mysqld/mysqld.sock 2>&1 |grep -q mysql && mysql -e "show databases" 2>&1 |grep -e mysql -e  performance_schema -e information_schema |wc -l |grep -q ^0$ || {
                                        export PLACE=system-mysql/ ;
                                        ## BACKUPS INTO A SINGLE REPO 
                                        [[ "${SINGLE_REPO}" = "true" ]]      && export PLACE="docker-mysql"
                                        [[ "${DOCKER_TO_SYSTEM}" = "true" ]] && export PLACE="system-mysql"

                                        export STAGE=SYSTEM-DB-MYSQL ;
                                        echo '===>SYS-MYQL: '${PLACE} | _logger_sys ;
                                        export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
					echo | restic init 2>&1 |grep -v -e "already initialized" -e "Please note that knowledge of your" -e "the repository. Losing your password means th" -e "rrecoverably lost" -e "password is correct"  2>&1 | _logger_pgsql ;
                                        restic unlock --cleanup-cache 2>&1 | _logger_sys ;
                _sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "BEFORE_MYSQL_DUMP"
                                        snapshots=$(echo | restic snapshots --json 2>/dev/null | TZ=GMT jq -r '.[-1].time' |sed 's/null/'${ZERODATE}'/g');snap_time_json=$(echo "$snapshots");
                                        if [ -z "$snap_time_json" ]; then snap_time_json="$ZERODATE";fi ;
                                        echo $(echo -n "before backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_sys
                                        snapshot_time=$(date -u -d "$snap_time_json" +%s);
                                        case $snapshot_time in
                                            ''|*[!0-9]*) export snapshot_time=0 ;;
                                            #*) echo good ;;
                                        esac
                                        snapshot_age=$(($(date -u +%s)-$snapshot_time));
                                        if test "$snapshot_age" -ge "${RESTIC_MINAGE}"  ;then
                                            echo "==>MYSQL: "$a" (last backup $snapshot_age seconds ago, threshold ${RESTIC_MINAGE})" | _logger_sys
                                            mysqldump --single-transaction --all-databases 2>/dev/shm/docker2restic.mysqldump.system.stderr.log  --single-transaction --all-databases  | gzip --rsyncable | restic backup --hostname "docker_database_$a" --stdin --stdin-filename mysql.sql.gz 2>&1 | _logger_mysql
                                        fi
                                        #forget even if we did not save

          ##forget old ones since restic policy fails
          rm /dev/shm/.restic_forget_list;restic unlock
          restic snapshots|grep "[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]"|sed 's/ \+/ /g'|cut -d" " -f1-3|while read line;do id=${line/ *};
                 shotdate=$(echo "$line"|cut -d" " -f2,3) ;
                 shotepoch=$(date -d "$shotdate" +%s) ;
                 shotagesec=$(($(date -u +%s )-$shotepoch));
                 echo -n "SNAP: $shotdate ( $shotagesec ) "| _logger_mysql;
                 [[ $shotagesec -ge $((86400*${KEEPDAYS}))  ]] && { echo -n "forget $id |"|_logger_mysql ;forgetlist="$forgetlist $id" ;  } ;
                 [[ $(echo $forgetlist|wc -w ) -ge 23 ]] && { restic forget $forgetlist |sed 's/$/|/g' |tr -d '\n'| _logger_mysql ;forgetlist="" ; }  ;
                 echo "$forgetlist" > /dev/shm/.restic_forget_list
                 echo;
                 done
          forgetlist=$(test -e /dev/shm/.restic_forget_list && cat /dev/shm/.restic_forget_list)
          echo $forgetlist|wc -w |grep -q ^0$ ||  { restic forget $forgetlist |sed 's/$/|/g' |tr -d '\n'| _logger_mysql ;forgetlist="" ; }
          forgetlist=""
          ## end forget
					(sleep 6; _renice_restic 5 ) & restic forget --keep-within ${KEEPDAYS}"d" 2>&1  | _stdout_delete_filter | _logger_mysql
					(sleep 6; _renice_restic 5 ) & restic ${NO_CACHE} prune 12>&1  |grep -v -e "password is correct"  2>&1 | _logger_mysql
					echo $(echo -n "after backup:" ; echo | restic snapshots 2>&1 |grep snapshots 2>&1 || echo "0 snapshots" ) | _logger_sys
					echo '////SYS-MYSQL' | _logger_mysql
          _sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER:MYSQL:DUMP"
          ## re-adjust penalty and cpulimits to current load
          LOADPENALTYPERCENT=$(_calculate_loadpenalty)
          ############################DEFAULTMAXPERC ...
          [[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
          CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
          ######################################################
  echo -n ; } ;

#_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics

#### CLEANUP STAGE
awscli=0
which awscli &>/dev/null && awscli=1

if [ "$awscli" -eq 1 ] ; then
echo "===>CLEANUP oprhan" | _logger_sys

touch /dev/shm/.restic_cleanup_running
while ( test -e /dev/shm/.restic_cleanup_running ) ;do
_stop_restic_on_load
    sleep 5
done &

_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "BEFORE_CLEANUP"
## re-adjust penalty and cpulimits to current load
LOADPENALTYPERCENT=$(_calculate_loadpenalty)
############################DEFAULTMAXPERC ...
[[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
######################################################

test -f /tmp//tmp/docker2restic_${startdate}.containers && rm /tmp//tmp/docker2restic_${startdate}.containers
which docker &>/dev/null && docker ps --format '{{.Names}}' &> /tmp//tmp/docker2restic_${startdate}.containers

echo ${RESTIC_AWS_URL} |grep -q ^s3 && {
    RESTIC_CUT_URL=${RESTIC_AWS_URL#s3:};
    ## list docker-database repos and clean them
    ## logged in sys conntext (sice running from system)
    AWS_DEFAULT_OUTPUT="text" aws --endpoint-url ${RESTIC_CUT_URL%/*} s3 ls "s3://"${RESTIC_AWS_URL##*/}"/docker/database/" |sed 's/ \+PRE //g;s/\/$//g' |while read resticrepo;do
                        a=$resticprepo
                        export PLACE="docker/database/$a"
                        ## REST_SERVER EXPECTS FLAT FORM( HAS ONE ROOT DIR)
                        echo "${RESTIC_AWS_URL}" |grep -q -e "http://" -e "https://" && export PLACE="docker_database_$a"
                        ## BACKUPS INTO A SINGLE REPO
                        [[ "${SINGLE_REPO}" = "true" ]]      && export PLACE="docker-mysql"
                        [[ "${DOCKER_TO_SYSTEM}" = "true" ]] && export PLACE="system-mysql"
                        export STAGE="CLEANUP-DOCKER-DB"
                        export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
                        ## not found in current list means orphaned
                        grep -v ^${resticrepo}$ /tmp/docker2restic_${startdate}.containers && {
                        [[ "${HAVE_CPULIMIT}" = "true" ]]  || GOGC=20 GOMAXPROCS=1 nice -n 19 ionice -c2 -n7                                         restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter | _logger_sys
                        [[ "${HAVE_CPULIMIT}" = "true" ]]  && GOGC=20 GOMAXPROCS=2 nice -n 19 ionice -c2 -n7 cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys
                        echo -n  ; } ;
                        ## this step is logged to the docker2restic.sys.log since we do not know what type of db we have and pruning orphans is a system job
                        done

    _sleep_on_high_load ;
    [[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER:CLEANUP:SYS"
    ## re-adjust penalty and cpulimits to current load
    LOADPENALTYPERCENT=$(_calculate_loadpenalty)
    ############################DEFAULTMAXPERC ...
    [[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
    CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
    ######################################################

    ## list docker-volume   repos and clean them
    AWS_DEFAULT_OUTPUT="text" aws --endpoint-url ${RESTIC_CUT_URL%/*} s3 ls "s3://"${RESTIC_AWS_URL##*/}"/docker/volumes/" |sed 's/ \+PRE //g;s/\/$//g' |while read resticrepo;do
                        a=$resticprepo
                        PLACE="docker/volumes/$a"
                        ## REST_SERVER EXPECTS FLAT FORM( HAS ONE ROOT DIR)
                        echo "${RESTIC_AWS_URL}" |grep -q -e "http://" -e "https://" && export PLACE="docker_volumes_$a"
                        ## BACKUPS INTO A SINGLE REPO 
                        [[ "${SINGLE_REPO}" = "true" ]]      && export PLACE="docker_volumes"
                        [[ "${DOCKER_TO_SYSTEM}" = "true" ]] && export PLACE="system"

                        export STAGE="CLEANUP-DOCKER-VOL"
                        export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
                        ## not found in current list means orphaned
                        grep -v ^${resticrepo}$ /tmp/docker2restic_${startdate}.containers && {
                        [[ "${HAVE_CPULIMIT}" = "true" ]]  || GOGC=20 GOMAXPROCS=1 nice -n 19 ionice -c2 -n7                                         restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys
                        [[ "${HAVE_CPULIMIT}" = "true" ]]  && GOGC=20 GOMAXPROCS=2 nice -n 19 ionice -c2 -n7 cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys
                                                                                                                                          echo -n  ; } ;
                              done
    _sleep_on_high_load ;
    [[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER:CLEANUP:DOCKER:SQL"

    ## re-adjust penalty and cpulimits to current load
    LOADPENALTYPERCENT=$(_calculate_loadpenalty)
    ############################DEFAULTMAXPERC ...
    [[ -z "${CPUMAXPERC}" ]] && CPUMAXPERC=$(_calculate_cpumaxperc)
    CPUMAXNUMERIC=$(awk "BEGIN { print  ${CPUMAXPERC}  / 100 }")
    ######################################################

    ## list system-mysql and clean them

    AWS_DEFAULT_OUTPUT="text" aws --endpoint-url ${RESTIC_CUT_URL%/*} s3 ls "s3://"${RESTIC_AWS_URL##*/}"/" |grep -e system-mysql -e system-pgsql |sed 's/ \+PRE //g;s/\/$//g' |while read resticrepo;do
        #export PLACE="system-database/$a"
        PLACE=$resticrepo
        export STAGE="CLEANUP-SYS-SQL"
        export RESTIC_REPOSITORY=${RESTIC_AWS_URL%/}"/"${PLACE#/}
        ## not found in current list means orphaned
        grep -v ^${resticrepo}$ /tmp/docker2restic_${startdate}.containers && {
                                                                                [[ "${HAVE_CPULIMIT}" = "true" ]]  || GOGC=20 GOMAXPROCS=1 nice -n 19 ionice -c2 -n7                                                       restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys
                                                                                [[ "${HAVE_CPULIMIT}" = "true" ]]  && GOGC=20 GOMAXPROCS=2 nice -n 19 ionice -c2 -n7 cpulimit --limit ${CPUMAXPERC} ${CPULIMIT_OPTIONS} -- restic forget --keep-within ${KEEPDAYS}"d" 2>&1 | _stdout_delete_filter  | _logger_sys
                                                                                echo -n  ; } ;
                                                                              ## this step is logged to the docker2restic.sys.log since we do not know what type of db we have and pruning orphans is a system job
                                                                              done
    _sleep_on_high_load ;
    [[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "AFTER:CLEANUP:SYS:SQL"
echo -n ;}; # END echo ${RESTIC_AWS_URL} |grep -q ^s3 && {

fi

test -e /dev/shm/.restic_cleanup_running && rm /dev/shm/.restic_cleanup_running 
enddate=$(date -u +%s)
seconds=$(($enddate-$startdate))
EXTIP=$( curl -s https://doh.my.cloudns.in/showip || curl https://ifconfig.co||curl -s whatismyip.akamai.com )
timing=$( TZ=UTC printf " FINISHED: %d days %(%H hours %M minutes %S seconds)T\n " $((seconds/86400)) $seconds |sed 's/: 0 days /: /g' )
echo $(hostname -f |head -c 32)" ${EXTIP} "${timing}| tee -a ${mylogdest} >/dev/null
_sleep_on_high_load ;
[[ "$DEBUGME" = "true" ]] && _debug_loadstatistics "$timing"

## send GUI info
which notify-send &>/dev/null && notify-send "Restic backup FINISHED $timing" 2>&1 | tee -a ${mylogdest} >/dev/null
grep -e Error -e ERROR -e error -e FAILED -e failed -e Failed ${mylogdest} -q && cat ${mylogdest}


MSGTXT=$(date)" Backup BOT $timing ( with Docker2Restic ) , log attached "
##webhook overrides telegram-notify
test -e /etc/backup-tg-webhook.conf && {  source /etc/backup-tg-webhook.conf ;

[[ -z "${RESTIC_HOSTNAME}" ]] && RESTIC_HOSTNAME=$(hostname -f |head -c 32)

grep -e Error -e ERROR -e error -e FAILED -e failed -e Failed ${mylogdest} -q && MSGTXT="(Errors) ""$MSGTXT"

[[ -z "${TGWEBBHOOKURL}" ]]   || echo "SND_NOTIF_TG"

[[ -z "${TGWEBBHOOKURL}" ]]   || send_tgwebhook_file "BACKUP@${RESTIC_HOSTNAME}" "$MSGTXT" "/tmp/docker2restic_${startdate}.log" 2>&1|grep -i -e error -e failed -e timeout ; } ;
                              
[[ -z "${GOTIFY_PRIO}" ]]     && GOTIFY_PRIO=3

[[ -z "${GOTIFY_ENDPOINT}" ]] || echo "SND_NOTIF_GOTIFY"

[[ -z "${GOTIFY_ENDPOINT}" ]] || send_gotify "${GOTIFY_ENDPOINT}" "BACKUP@${RESTIC_HOSTNAME}" "$MSGTXT" "${GOTIFY_PRIO}"


test -e /etc/backup-tg-webhook.conf || {

which telegram-notify &>/dev/null && (
                                     #echo TG_NOTIFY_FOUND
                                     echo ${TELEGRAM_CONFIG} |grep -q ^OK$ && (
                                     echo "tg_sending:"
                                     grep -e Error -e ERROR -e error -e FAILED -e failed -e Failed ${mylogdest} -q && (echo "(Errors)""$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/docker2restic_${startdate}.log --warning --text -)  || ( echo "$MSGTXT" | telegram-notify --config ${TGCONFIG_FILE} --silent --document /tmp/docker2restic_${startdate}.log --text - )
     ) )
echo -n ; } ;
rm ${mylogdest}
test -f /tmp//tmp/docker2restic_${startdate}.containers &&  rm /tmp//tmp/docker2restic_${startdate}.containers
test -f /tmp/docker2restic_${startdate}.mounts.list && rm /tmp/docker2restic_${startdate}.mounts.list
exit 0
