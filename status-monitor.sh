#!/bin/bash
which lsof >/dev/null || exit "NEED LSOF , busybox or real"
have_pidof=0
which pidof >/dev/null && have_pidof=1


_shorten_dots() { while read longname;do echo $(echo "$longname"|head -c $1 )"..."$(echo "$longname"|tail -c $2) ;done ;  } ;

_filenamefilter() { cat | grep -v -e /tmp/ -e '/root/.cache/restic'  -e /usr/lib -e /proc -e /dev -e /sys -e /bin/bash -e socket: -e pipe: -e anon_inode: -e /dev/null|grep "/"|sed 's/.\+bin\/restic//g'|grep -v -e docker2restic -e "/dev/pts" -e "/tmp/restic-temp"|cut -d"/" -f2- |grep "/" | _shorten_dots 14 16 ; } ;

_docker_restic_mon() {
                      echo -n $(date )"→cache :"$(docker exec docker2restic du -h -s /root/.cache|sed 's/\/root\/.cache//g')" "$(docker exec docker2restic /bin/sh -c "rpid=$(ps ax -o fname,pid | sed -e '/restic /!d;s/.* //');"'lsof -p $rpid ' |_filenamefilter ) ;
                      echo -ne '\r'   ;
                      } ;

_system_restic_mon() { 
                        if [ "$have_pidof" -eq 1 ] ; then
                           for mypid in $(pidof restic 2>/dev/null);do 
                             echo -n $(date )"restic(pid "$mypid" )→cache :"$(du -h -s /root/.cache/restic|sed 's/\/root\/.cache\/restic//g')" "$(lsof -p $mypid | _filenamefilter )
                             echo -ne '\r'   ;
                            done
                                                     
                        else

                          for mypid in $(ps ax -o fname,pid | sed -e '/restic /!d;s/.* //');do 
                            echo -n $(date )"restic(pid "$mypid" )→cache :"$(du -h -s /root/.cache/restic|sed 's/\/root\/.cache\/restic//g')" "$(lsof -p $mypid | _filenamefilter )
                             
                          done ;
                        fi ;
                        } ;
while (true);do 
  
      which docker >/dev/null && docker ps |grep -q docker2restic &&  ( _docker_restic_mon  ) || ( _system_restic_mon 1>&2 )
      echo -ne "           ←"'\r'
      sleep 6;
   done
